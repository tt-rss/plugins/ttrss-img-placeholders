/* global require, PluginHost */

require(['dojo/_base/kernel', 'dojo/ready'], function  (dojo, ready) {
	function set_placeholder(img) {

		img.style.backgroundColor = '#bbb';
		img.style.backgroundImage = 'url(plugins.local/af_img_placeholders/loading.png)';
		img.style.backgroundPosition = 'center center';
		img.style.backgroundRepeat = 'no-repeat';
		img.style.minWidth = '300px';
		img.style.minHeight = '300px';

		img.onload = function() {
			img.style.minWidth = '';
			img.style.minHeight = '';
		}

		img.onerror = function() {
			this.style.backgroundColor = '';
			this.style.backgroundImage = '';
		}

	}

	function set_placeholders(row) {
		[...row.querySelectorAll("img")].forEach((img) => set_placeholder(img));
	}

	ready(function () {
		PluginHost.register(PluginHost.HOOK_ARTICLE_RENDERED_CDM, function (row) {
			set_placeholders(row.querySelector(".content"));
			return true;
		});

		PluginHost.register(PluginHost.HOOK_ARTICLE_RENDERED, function (row) {
			set_placeholders(row);
			return true;
		});
	});
});
