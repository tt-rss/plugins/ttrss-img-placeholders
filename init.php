<?php
class Af_Img_PlaceHolders extends Plugin {

	function about() {
		return array(null,
			"Add loading placeholders for images (for slow connections)",
			"fox");
	}

	function init($host) {
		//
	}

	function get_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function api_version() {
		return 2;
	}

}
